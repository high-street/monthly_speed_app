library(shiny)
library(dplyr)
library(openxlsx)
library(tidyr)
library(shinyjs)
library(writexl)

speeds_final <- data.frame()

####### UI ----- 
ui <- fixedPage(
  
  tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "custom.css")), 
  
  useShinyjs(),
  title = "Monthly Speed Report App",
  
  fluidRow(id = "header",
           h1("FHWA DVC Monthly Speed Report")
           ),
  fluidRow(align = "center", id = "main-page",
           actionButton("info", "", icon = icon("info")),
           column(4),
           column(4,
                  fluidRow(id = "upload_div",
                    fileInput("file_upload", "Upload Raw File", accept = ".xlsx"),
                    a(href = "user_guide.docx", "User Guide", download = NA, 
                           target = "_blank"),
                    downloadLink("download_data", "")
                    )
                  ),
           column(4)
  ),
  
  hidden(absolutePanel(
    id = "info-module",
    fluidRow(align = "right", style = "margin-right: 10px; margin-top: 10px; padding: 0px;",
             actionButton("close", label = "", icon = icon("times"), class = "button", 
                          style = "background-color: #FF6961; border-radius: 50%;")),
    h2("Information", style = "text-align: center;"),
    p("This app is designed to take the raw Monthly Traffic Speeds Excel report 
      and perform cleaning analysis to update the
      FHWA Monthly Traffic Speed Dashboards. The output of this report should
      be uploaded to the Tableau Dashboard for the monthly speed report. The expected
      file to be uploaded is attached here (",
      a(href="TST_Index_Report_June2020.xlsx", "Expected File", download = NA,
        target = "_blank"), ")."), 
    
  )),
  
  ###### Footer ----
  fluidRow(id = "footer",
           p("Built by: High Street Consulting Group",
             tags$a(href = "http://www.highstreetconsulting.com",
                    target = "_blank",
                    img(src = "hs_logo_square.png", height = "30px"))),
  )
)

####### Server -----

server <- function(session, input, output) {
  
  finalData <- reactiveValues()
  
  observeEvent(input$file_upload, {
    
    req(input$file_upload$datapath)
    
    sheets <- c("All", "Passenger", "Truck")
    
    speed_data <- list()
    for (sheet in 1:length(sheets)) {
      data <- read.xlsx(isolate(input$file_upload$datapath), 
                        sheet = sheet, 
                        fillMergedCells = TRUE, colNames = TRUE)
      speed_data[[sheets[sheet]]] <- data
    }
    
    ############################# CONSTANTS ########################################
    
    FUNCTIONAL_CLASS <- data.frame(
      `Functional Class` = 1:7, 
      `Functional Name` = c("Interstate",
                            "Other Freeways and Expressways",
                            "Other Principal Arterial",
                            "Minor Arterial",
                            "Major Collector",
                            "Minor Collector",
                            "Local")
    )
    
    FUNCTIONAL_CLASS$`Functional Classification` <- paste0(FUNCTIONAL_CLASS$Functional.Class,
                                                           " - ",
                                                           FUNCTIONAL_CLASS$Functional.Name)
    
    ########################### Script #############################################
    final_speed_data <- data.frame()
    
    for (sheet in sheets) {
      data_init <- speed_data[[sheet]]
      first_data_end <- which(data_init == "2020 Monthly Travel Speed Trend", arr.ind = FALSE)[[1]] - 1
      second_data_end <- which(data_init == "2020 Monthly Travel Speed Changes (2020-2019)/2019 %", arr.ind = FALSE)[[1]] - 1
      data_2019 <- data_init[1:first_data_end, ]
      data_2020 <- data_init[first_data_end:second_data_end, ]
      
      colnames(data_2019) <- data_2019[2, ]
      colnames(data_2020) <- data_2019[2, ]
      data_2019 <- data_2019[-c(1:3), -17]
      data_2020 <- data_2020[-c(1:2), -17]
      
      data_2019 <- data_2019 %>%
        mutate(year = 2019) %>%
        pivot_longer(cols = all_of(toupper(month.abb)), names_to = "month", values_to = "Speed") %>% 
        mutate(`Functional Class` = as.numeric(`Functional Class`)) %>% 
        left_join(FUNCTIONAL_CLASS, by = c("Functional Class" = "Functional.Class")) %>% 
        mutate(month_num = rep(1:12, 30))
      
      data_2020 <- data_2020 %>%
        mutate(year = 2020) %>%
        pivot_longer(cols = all_of(toupper(month.abb)), names_to = "month", values_to = "Speed") %>% 
        mutate(`Functional Class` = as.numeric(`Functional Class`)) %>% 
        left_join(FUNCTIONAL_CLASS, by = c("Functional Class" = "Functional.Class")) %>% 
        mutate(month_num = rep(1:12, 30))
      
      data <- bind_rows(data_2019, data_2020) %>% 
        mutate(Vehicles = sheet) %>% 
        filter(!is.na(Speed)) %>%
        rename(`Time Frame` = `Time Period.1`)
      
      unique_rows <- (length(unique(data$month)) * 7 * 2) + length(unique(data$month))
      data$Time_Of_Day <- c(
        rep("AM", unique_rows),
        rep("PM", unique_rows),
        rep("AM", unique_rows),
        rep("PM",unique_rows)
      )
      
      final_speed_data <- bind_rows(final_speed_data, data)
    }
    
    speed_key <- final_speed_data %>% 
      mutate(key = paste(Vehicles, Time_Of_Day, `Area Type`),
             next_month = month_num + 1,
             next_year = year + 1,
             start_month = 1) %>% 
      select(key, year, `Functional Class`, 
             month_num, next_month, next_year, start_month, Speed)
    
    speeds_final <- final_speed_data %>% 
      mutate(key = paste(Vehicles, Time_Of_Day, `Area Type`)) %>% 
      left_join(speed_key, by = c("key", "year", "Functional Class", "month_num" = "next_month"), 
                suffix = c("", ".last_month")) %>% 
      select(-next_year, -start_month, -month_num.last_month) %>%
      left_join(speed_key, by = c("key", "year" = "next_year", "month_num",
                                  "Functional Class"),
                suffix = c("", ".last_year")) %>% 
      select(-next_month, -start_month, -year.last_year) %>%
      left_join(select(filter(speed_key, month_num == 1), key, year, `Functional Class`, Speed),
                by = c("key", "year", "Functional Class"), suffix = c("", ".from_jan"))
    
    finalData$data <- speeds_final
    shinyjs::runjs("$('#download_data')[0].click();")
    
  })
  
  output$download_data <- downloadHandler(
    filename = "clean_monthly_speed_report.csv",
    content = function(file) {
      write.csv(finalData$data, file, row.names = FALSE)
    }
  )
  
  observeEvent(input$info, { shinyjs::show("info-module") })
  observeEvent(input$close, { shinyjs::hide("info-module") })
}

shinyApp(ui, server)